from angina_storage import get_angina_data
from classical_ml.estimation_models_persistence import get_best_estimation_model
from free_form_ga.free_form_ga_solution import run_ga_free_form


model = get_best_estimation_model()
X, y = get_angina_data()


run_ga_free_form(X=X, y=y)