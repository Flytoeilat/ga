import numpy as np

from angina_storage import get_angina_data_shuffled
from free_form_ga.free_form_ga_solution import run_ga_free_form

scores = []
for i in range(10):
    X, y = get_angina_data_shuffled()
    scores.append(run_ga_free_form(X=X, y=y))

print(np.mean(scores))
