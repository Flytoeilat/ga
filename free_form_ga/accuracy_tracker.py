class AccurayTracker(object):
    def __init__(self, score_threshold, dataset_length):
        self.score_threshold = score_threshold
        self.dataset_length = dataset_length
        self.true_pred = 0

    def collect(self, score, expected_label):
        if score > self.score_threshold:
            label = 1
        else:
            label = 0
        if label == expected_label:
            self.true_pred += 1

    def get_accuracy(self):
        return self.true_pred / self.dataset_length
