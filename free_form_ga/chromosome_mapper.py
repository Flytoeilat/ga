import math

_epsilon = 0.001
_map = {
    1: math.sin,
    2: math.cos,
    3: math.tan,
    4: math.exp,
    5: lambda x: x ** 2,
    6: lambda x: x ** 3,
    7: lambda x: x ** 4,
    8: lambda x: 1 / (x + _epsilon),
    9: lambda x: 1 / (x ** 2 + _epsilon),
    10: math.tanh,
    11: abs,
    12: lambda x: 1 / (1 + math.exp(-x)),
    13: lambda x: max(x, 0),
    14: lambda x: math.log2(abs(x + _epsilon)),
    15: lambda x: math.sqrt(abs(x)),
    16: lambda x: -x
}


def chromosome_to_funcs(chromosome):
    return [_map[i] for i in chromosome]
