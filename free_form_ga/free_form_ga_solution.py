from free_form_ga.accuracy_tracker import AccurayTracker
from free_form_ga.chromosome_mapper import chromosome_to_funcs
from gaft import GAEngine
from gaft.analysis.fitness_store import FitnessStore
from gaft.components import Population, DecimalIndividual
from gaft.operators import RandomIntMutation, LinearRankingSelection, \
    TwoPointCrossover
from gaft.plugin_interfaces.analysis import OnTheFlyAnalysis


def run_ga_free_form(X, y):

    indv_template = DecimalIndividual(ranges=[(1, 16)] * 10, eps=1)
    population = Population(indv_template=indv_template, size=100)
    population.init()

    selection = LinearRankingSelection()
    crossover = TwoPointCrossover(pc=1.00, pe=0.5)
    mutation = RandomIntMutation(pm=0.2)

    engine = GAEngine(population=population, selection=selection,
                      crossover=crossover, mutation=mutation,
                      analysis=[FitnessStore])

    @engine.fitness_register
    def fitness(indv):
        at = AccurayTracker(score_threshold=4, dataset_length=len(X))

        funcs = chromosome_to_funcs(indv.solution)
        for i, features in X.iterrows():
            score = 0
            for func, feature_val in zip(funcs, features):
                score += func(feature_val)

            at.collect(score, expected_label=y[i])

        return at.get_accuracy()

    @engine.analysis_register
    class ConsoleOutputAnalysis(OnTheFlyAnalysis):
        interval = 1
        master_only = True

        def register_step(self, g, population, engine):
            msg = 'Generation: {}, best fitness: {:.3f}'.format(g, engine.ori_fmax)
            self.logger.info(msg)

        def finalize(self, population, engine):
            best_indv = population.best_indv(engine.fitness)
            x = best_indv.solution
            y = engine.ori_fmax
            msg = 'Optimal solution: ({}, {})'.format(x, y)
            self.logger.info(msg)

    engine.run(ng=300)
    return engine.ori_fmax
