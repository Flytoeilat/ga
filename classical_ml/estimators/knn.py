from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsClassifier

_grid_search_params = {'n_neighbors': list(range(1, 11)),
                       'algorithm': ['auto', 'ball_tree', 'kd_tree', 'brute'],
                       'weights': ['uniform', 'distance'],
                       'p': [1, 2, 3]}


def register_best_estimator(X, y, cv, estimators):
    grid = GridSearchCV(KNeighborsClassifier(), _grid_search_params, cv=cv)
    grid.fit(X, y)
    estimators.append({'name': 'KNN', 'estimator': grid.best_estimator_})
