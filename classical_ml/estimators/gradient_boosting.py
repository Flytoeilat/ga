from sklearn.ensemble import GradientBoostingClassifier
from sklearn.model_selection import GridSearchCV

_grid_search_params = {'loss': ['deviance', 'exponential'],
                       'learning_rate': [0.001, 0.01, 0.1],
                       'n_estimators': [100, 250, 500],
                       'max_depth': [2, 3, 4, 5]}


def register_best_estimator(X, y, cv, estimators):
    grid = GridSearchCV(GradientBoostingClassifier(), _grid_search_params, cv=cv)
    grid.fit(X, y)
    estimators.append({'name': 'Gradient Boosting', 'estimator': grid.best_estimator_})
