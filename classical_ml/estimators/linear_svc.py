from sklearn.model_selection import GridSearchCV
from sklearn.svm import LinearSVC

_grid_search_params = {'C': [0.01, 0.1, 1, 10, 100],
                       'dual': [True, False]}


def register_best_estimator(X, y, cv, estimators):
    grid = GridSearchCV(LinearSVC(), _grid_search_params, cv=cv)
    grid.fit(X, y)
    estimators.append({'name': 'Linear SVC', 'estimator': grid.best_estimator_})
