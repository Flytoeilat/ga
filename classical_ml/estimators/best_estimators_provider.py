from classical_ml.estimators import gradient_boosting, knn, linear_svc, svc

_estimator_registrators = [gradient_boosting, knn, linear_svc, svc]


def get_best_estimators(X, y, cv):
    estimators = []
    for reg in _estimator_registrators:
        reg.register_best_estimator(X, y, cv, estimators)

    return estimators
