from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC

_grid_search_params = {'gamma': ['scale', 'auto'],
                       'C': [0.01, 0.1, 1, 10, 100, 1000],
                       'kernel': ['linear', 'poly', 'rbf', 'sigmoid']}


def register_best_estimator(X, y, cv, estimators):
    estimators.append({'name': 'SVC', 'estimator': get_best_estimator(X, y, cv)})


def get_best_estimator(X, y, cv):
    grid = GridSearchCV(SVC(), _grid_search_params, cv=cv)
    grid.fit(X, y)
    return grid.best_estimator_
