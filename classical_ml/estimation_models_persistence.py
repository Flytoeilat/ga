import pickle
import pandas as pd

from config import ROOT
from os_extensions import join_path

_best_estimation_model_file_name = join_path(ROOT, 'classical_ml', '_data', 'best_estimation_model.pickle')
_all_estimators_result_file_name = join_path(ROOT, 'classical_ml', '_data', 'all_estimators_results.csv')


def save_all_estimators_results(df):
    df.to_csv(_all_estimators_result_file_name, index=False)


def get_all_estimators_results():
    return pd.read_csv(_all_estimators_result_file_name)


def save_best_estimation_model(model):
    with open(_best_estimation_model_file_name, 'wb') as fwh:
        pickle.dump(model, fwh)


def get_best_estimation_model():
    with open(_best_estimation_model_file_name, 'rb') as frh:
        return pickle.load(frh)
