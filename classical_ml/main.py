from angina_storage import get_angina_data
from classical_ml.estimation_model_creator import create_estimation_model
from classical_ml.estimation_models_persistence import get_all_estimators_results
from classical_ml.estimators_plotter import save_bar_plot
from config import ROOT
from os_extensions import join_path

cv = 5
plot_file_name = join_path(ROOT, 'classical_ml', '_data', 'estimation_models_comparison.png')

X, y = get_angina_data()

create_estimation_model(X, y, cv)
save_bar_plot(get_all_estimators_results(),
              title='Angina Data Set - Estimators Comparison',
              plot_flie_name=plot_file_name)
