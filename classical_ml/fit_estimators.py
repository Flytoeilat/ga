import pandas as pd
from sklearn.model_selection import cross_val_score


def fit_estimators(X, y, cv, estimators):
    results = []
    for estimator in estimators:
        for score in cross_val_score(estimator['estimator'], X, y, cv=cv):
            results.append([estimator['name'], score])

    return pd.DataFrame(data=results, columns=['estimator', 'accuracy'])



