import seaborn as sns

sns.set_style('whitegrid')


def save_bar_plot(estimators_results_df, title, plot_flie_name):
    bp = sns.barplot(x='estimator', y='accuracy', data=estimators_results_df, ci=None)
    bp.set_title(title)

    for p in bp.patches:
        bp.annotate(format(p.get_height(), '.2f'), (p.get_x() + p.get_width() / 2., p.get_height()), ha='center',
                    va='center', xytext=(0, 10), textcoords='offset points')

    bp.figure.savefig(plot_flie_name)