from classical_ml.estimation_models_persistence import save_best_estimation_model, save_all_estimators_results
from classical_ml.estimators.best_estimators_provider import get_best_estimators
from classical_ml.fit_estimators import fit_estimators


def create_estimation_model(X, y, cv):
    estimators = get_best_estimators(X, y, cv=cv)

    fit_results_df = fit_estimators(X, y, cv, estimators)

    save_all_estimators_results(fit_results_df)
    save_best_estimation_model(_best_estimator(estimators, fit_results_df))


def _best_estimator(estimators, fit_results_df):
    best_estimator_name = fit_results_df.groupby('estimator').mean().idxmax().values[0]
    return next((e['estimator'] for e in estimators if e['name'] == best_estimator_name))

