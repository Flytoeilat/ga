from angina_storage import get_angina_data_shuffled
from classical_ml.estimators.svc import get_best_estimator


def get_svc_models_and_df(iter=10):
    for i in range(iter):
        X, y = get_angina_data_shuffled()
        model = get_best_estimator(X, y, cv=5)
        yield model, X, y
