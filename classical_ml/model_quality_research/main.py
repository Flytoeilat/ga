from sklearn.model_selection import cross_val_score
from classical_ml.model_quality_research.svc_models_and_df_provider import get_svc_models_and_df

number_of_iter = 10
acc = []

for (model, X, y) in get_svc_models_and_df(iter=10):
    scores = cross_val_score(model, X, y, cv=5)
    acc.append(sum(scores) / len(scores))

print('svc model quality result')
print('accuracy %f' % (sum(acc) / len(acc)))