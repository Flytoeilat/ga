import pandas as pd

from config import ROOT
from data_preprocessor import DataPreProcessor
from os_extensions import join_path

_data_set_file = join_path(ROOT, 'angina_data_set.csv')

_labels_col = 'Angina'
_numerical_cols = ['Age', 'Cigarettes']
_categorical_cols = ['Smoke', 'Hypertension', 'Diabetes', 'FH Angina', 'FH MI', 'FH Stroke']

dp = DataPreProcessor(pd.read_csv(_data_set_file, sep='\t'), labels_col=_labels_col)

_features, _labels = dp.drop_cols(['ID']) \
    .drop_rows_with_any_na() \
    .scale(cols=_numerical_cols) \
    .cat_to_one_hot(cols=_categorical_cols) \
    .label_as_binary() \
    .get_data()

dp = DataPreProcessor(pd.read_csv(_data_set_file, sep='\t'), labels_col=_labels_col)
_features_no_scale, _labels_no_scale = dp.drop_cols(['ID']) \
    .drop_rows_with_any_na() \
    .cat_to_one_hot(cols=_categorical_cols) \
    .label_as_binary() \
    .get_data()


def get_angina_data(scale=True):
    if scale:
        return _features, _labels
    else:
        return _features_no_scale, _labels_no_scale


def get_angina_data_shuffled(scale=True):
    X, y = get_angina_data(scale)
    return X, y.sample(frac=1)
