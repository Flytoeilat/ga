import os


def join_path(path, *paths):
    return os.path.join(path, *paths)

