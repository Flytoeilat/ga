from angina_storage import get_angina_data
from classical_ml.estimation_models_persistence import get_best_estimation_model
from ga_ml.ga_ml_solution import run_ga_ml


model = get_best_estimation_model()
X, y = get_angina_data()


run_ga_ml(model=model, X=X, y=y)
