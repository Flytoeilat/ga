class GAConfig(object):
    def __init__(self, selection, crossover, mutation, population):
        self.population = population
        self.selection = selection
        self.crossover = crossover
        self.mutation = mutation
