import numpy as np
from sklearn.model_selection import cross_val_score
from ga_ml.ga_configs_provider import get_all_ga_configs

from gaft import GAEngine
from gaft.plugin_interfaces.analysis import OnTheFlyAnalysis
from gaft.analysis.fitness_store import FitnessStore


def run_ga_ml(model, X, y):

    for config in get_all_ga_configs():
        engine = GAEngine(population=config.population, selection=config.selection,
                          crossover=config.crossover, mutation=config.mutation,
                          analysis=[FitnessStore])

        @engine.fitness_register
        def fitness(indv):
            chromosome = indv.solution
            chromosome_X = chromosome * X
            scores = cross_val_score(model, chromosome_X, y, cv=5)
            accuracy = np.array(scores).mean()
            return float(accuracy)

        @engine.analysis_register
        class ConsoleOutputAnalysis(OnTheFlyAnalysis):
            interval = 1
            master_only = True

            def register_step(self, g, population, engine):
                msg = 'Generation: {}, best fitness: {:.3f}'.format(g, engine.ori_fmax)
                self.logger.info(msg)

            def finalize(self, population, engine):
                best_indv = population.best_indv(engine.fitness)
                x = best_indv.solution
                y = engine.ori_fmax
                msg = 'Optimal solution: ({}, {})'.format(x, y)
                self.logger.info(msg)

        engine.run(ng=300)
        return engine.ori_fmax
