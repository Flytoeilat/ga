from ga_ml.ga_config import GAConfig
from gaft.components import DecimalIndividual, Population
from gaft.operators import TournamentSelection, RouletteWheelSelection, LinearRankingSelection, \
    ExponentialRankingSelection
from gaft.operators import UniformCrossover, TwoPointCrossover, SinglePointCrossover
from gaft.operators import FlipBitBigMutation

size = 100

_ranges = [(0, 1), (-1, 1), (-2, 2), (0, 10)]

_selections = [RouletteWheelSelection(),
               TournamentSelection(),
               LinearRankingSelection(),
               ExponentialRankingSelection()
               ]
_crossovers = [UniformCrossover(pc=1.0, pe=0.5),
               UniformCrossover(pc=0.95, pe=0.5),
               UniformCrossover(pc=0.90, pe=0.7),
               UniformCrossover(pc=0.90, pe=0.6),
               UniformCrossover(pc=0.90, pe=0.5),
               UniformCrossover(pc=0.90, pe=0.4),
               UniformCrossover(pc=0.90, pe=0.3),
               SinglePointCrossover(pc=0.85, pe=0.7),
               UniformCrossover(pc=0.85, pe=0.6),
               TwoPointCrossover(pc=0.85, pe=0.5),
               UniformCrossover(pc=0.85, pe=0.4),
               UniformCrossover(pc=0.85, pe=0.3),
               UniformCrossover(pc=0.80, pe=0.7),
               UniformCrossover(pc=0.80, pe=0.6),
               UniformCrossover(pc=0.80, pe=0.5),
               UniformCrossover(pc=0.80, pe=0.4),
               UniformCrossover(pc=0.80, pe=0.3),
               UniformCrossover(pc=0.75, pe=0.7),
               UniformCrossover(pc=0.75, pe=0.6),
               UniformCrossover(pc=0.75, pe=0.5),
               UniformCrossover(pc=0.75, pe=0.4),
               UniformCrossover(pc=0.75, pe=0.3),
               UniformCrossover(pc=0.70, pe=0.7),
               UniformCrossover(pc=0.70, pe=0.6),
               TwoPointCrossover(pc=0.70, pe=0.5),
               UniformCrossover(pc=0.70, pe=0.4),
               UniformCrossover(pc=0.70, pe=0.3)
               ]
_mutations = [
    FlipBitBigMutation(pm=0.3, pbm=0.9, alpha=0.9),
    FlipBitBigMutation(pm=0.3, pbm=0.7, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.6, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.7, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.2, pbm=0.8, alpha=0.65),
    FlipBitBigMutation(pm=0.2, pbm=0.5, alpha=0.75),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.6),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.80),
    FlipBitBigMutation(pm=0.1, pbm=0.5, alpha=0.60)]


def get_all_ga_configs():
    for s in _selections:
        for c in _crossovers:
            for m in _mutations:
                for r in _ranges:
                    indv = DecimalIndividual(ranges=[r] * 10, eps=0.001)
                    p = Population(indv_template=indv, size=size).init()
                    p.individuals[0].chromsome = [1] * 10
                    p.individuals[0].solution = [1] * 10
                    yield GAConfig(s, c, m, p)
