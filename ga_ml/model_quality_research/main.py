import numpy as np

from classical_ml.model_quality_research.svc_models_and_df_provider import get_svc_models_and_df
from ga_ml.ga_ml_solution import run_ga_ml


scores = []
for model, X, y in get_svc_models_and_df(10):
    scores.append(run_ga_ml(model=model, X=X, y=y))
print(np.mean(scores))

