#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from config import ROOT
from os_extensions import join_path
from ..plugin_interfaces.analysis import OnTheFlyAnalysis


class FitnessStore(OnTheFlyAnalysis):
    ''' Built-in on-the-fly analysis plugin class for storing fitness related data during iteration.

    Attribute:
        interval(:obj:`int`): The analysis interval in evolution iteration, default 
                              value is 1 meaning analyze every step.
        master_only(:obj:`bool`): Flag for if the analysis plugin is only effective 
                                  in master process. Default is True.
    '''

    # Analysis interval.
    interval = 1

    # Only analyze in master process?
    master_only = True

    def setup(self, ng, engine):
        # Generation numbers.
        self.ngs = []

        # Best fitness in each generation.
        self.fitness_values = []

        # Best solution.
        self.solution = []

    def register_step(self, g, population, engine):
        # Collect data.
        best_indv = population.best_indv(engine.fitness)
        best_fit = engine.ori_fmax

        self.ngs.append(g)
        self.solution.append(best_indv.solution)
        self.fitness_values.append(best_fit)

    def finalize(self, population, engine):
        out_file_name = _get_file_name(self.fitness_values[-1])
        with open(out_file_name, 'w', encoding='utf-8') as f:
            header = _build_record_file_header(engine)
            f.write(header + '\n')
            f.write('best_fit = [\n')
            for ng, x, y in zip(self.ngs, self.solution, self.fitness_values):
                f.write('    ({}, {}, {}),\n'.format(ng, x, y))
            f.write(']\n\n')

        self.logger.info('Best fitness values are written to best_fit.py')


def _get_file_name(best_fit_value):
    file_name = '%s_%f.txt' % (datetime.now().strftime("%d-%m-%H.%M.%S"), best_fit_value)
    return join_path(ROOT, 'free_form_ga', 'records', file_name)


def _build_record_file_header(engine):
    range = engine.population.individuals[0].ranges[0]
    select = engine.selection.__class__.__name__
    cross = engine.crossover.__class__.__name__
    cross_pc = engine.crossover.pc
    cross_pe = engine.crossover.pe
    mutat = engine.mutation.__class__.__name__
    mutat_pm = engine.mutation.pm
    mutat_pbm = -1.0
    mutat_alpha = -1.0
    if hasattr(engine.mutation, 'pbm'):
        mutat_pbm = engine.mutation.pbm
        mutat_alpha = engine.mutation.alpha
    return """ranges of every chromosome : %s
    selection method is: %s
    crossover method is: %s
    pc: The probability of crossover: %f
    pe: Gene exchange probability: %f
    mutation method is: %s
    pm: The probability of mutation: %f
    pbm: The probability of big mutation: %f
    alpha: intensive factor: %f""" % (range, select, cross, cross_pc, cross_pe, mutat, mutat_pm, mutat_pbm, mutat_alpha)
