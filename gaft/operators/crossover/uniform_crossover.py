#!/usr/bin/env python
# -*- coding: utf-8 -*-

''' Uniform Crossover operator implementation. '''

import random
from copy import deepcopy

from ...plugin_interfaces.operators.crossover import Crossover


class UniformCrossover(Crossover):
    ''' Crossover operator with uniform crossover algorithm,
    see https://en.wikipedia.org/wiki/Crossover_(genetic_algorithm)

    :param pc: The probability of crossover (usaully between 0.25 ~ 1.0)
    :type pc: float in (0.0, 1.0]

    :param pe: Gene exchange probability.
    :type pe: float in range (0.0, 1.0]
    '''

    def __init__(self, pc, pe=0.5):
        if pc <= 0.0 or pc > 1.0:
            raise ValueError('Invalid crossover probability')
        self.pc = pc

        if pe <= 0.0 or pe > 1.0:
            raise ValueError('Invalid genome exchange probability')
        self.pe = pe

    def cross(self, father, mother):
        ''' Cross chromsomes of parent using uniform crossover method.

        :param population: Population where the selection operation occurs.
        :type population: :obj:`gaft.components.Population`

        :return: Selected parents (a father and a mother)
        :rtype: list of :obj:`gaft.components.IndividualBase`
        '''
        do_cross = True if random.random() <= self.pc else False

        if not do_cross:
            return father.clone(), mother.clone()

        # Chromsomes for two children.
        chrom1 = deepcopy(father.chromsome)
        chrom2 = deepcopy(mother.chromsome)

        for i, (g1, g2) in enumerate(zip(chrom1, chrom2)):
            do_exchange = True if random.random() < self.pe else False
            if do_exchange:
                chrom1[i], chrom2[i] = g2, g1

        child1, child2 = father.clone(), father.clone()
        child1.init(chromsome=chrom1)
        child2.init(chromsome=chrom2)

        return child1, child2


class SinglePointCrossover(Crossover):
    def __init__(self, pc, pe=0.5):
        if pc <= 0.0 or pc > 1.0:
            raise ValueError('Invalid crossover probability')
        self.pc = pc

        if pe <= 0.0 or pe > 1.0:
            raise ValueError('Invalid genome exchange probability')
        self.pe = pe

    @staticmethod
    def get_single_point_position(chromose_length):
        return random.randint(int(chromose_length * 0.3), int(chromose_length * 0.7))

    def cross(self, father, mother):

        do_cross = True if random.random() <= self.pc else False

        if not do_cross:
            return father.clone(), mother.clone()

        single_point_position = SinglePointCrossover.get_single_point_position(len(father.chromsome))

        chrom1 = father.chromsome[:single_point_position]
        chrom1.extend(mother.chromsome[single_point_position:])

        chrom2 = mother.chromsome[:single_point_position]
        chrom2.extend(father.chromsome[single_point_position:])

        child1, child2 = father.clone(), father.clone()
        child1.init(chromsome=chrom1)
        child2.init(chromsome=chrom2)

        return child1, child2


class TwoPointCrossover(Crossover):
    def __init__(self, pc, pe=0.5):
        if pc <= 0.0 or pc > 1.0:
            raise ValueError('Invalid crossover probability')
        self.pc = pc

        if pe <= 0.0 or pe > 1.0:
            raise ValueError('Invalid genome exchange probability')
        self.pe = pe

    @staticmethod
    def get_two_point_position(chromose_length):
        left_point = random.randint(int(chromose_length * 0.3), int(chromose_length * 0.4))
        right_point = random.randint(int(chromose_length * 0.7), int(chromose_length * 0.8))
        return left_point, right_point

    def cross(self, father, mother):

        do_cross = True if random.random() <= self.pc else False

        if not do_cross:
            return father.clone(), mother.clone()

        left_point, right_point = TwoPointCrossover.get_two_point_position(len(father.chromsome))

        chrom1 = father.chromsome[:left_point] + mother.chromsome[left_point:right_point] + father.chromsome[
                                                                                            right_point:]

        chrom2 = mother.chromsome[:left_point] + father.chromsome[left_point:right_point] + mother.chromsome[
                                                                                            right_point:]

        child1, child2 = father.clone(), father.clone()
        child1.init(chromsome=chrom1)
        child2.init(chromsome=chrom2)

        return child1, child2
