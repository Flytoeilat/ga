import pandas as pd
from sklearn.preprocessing import StandardScaler, MinMaxScaler


class DataPreProcessor(object):
    def __init__(self, df, labels_col):
        self.df = df
        self.labels_col = labels_col

    def drop_cols(self, cols):
        self.df.drop(columns=cols, inplace=True)
        return self

    def drop_rows_with_any_na(self):
        self.df = self.df.dropna().reset_index(drop=True)
        return self

    def label_as_binary(self, one_label='yes'):
        self.df[self.labels_col] = self.df[self.labels_col].apply(lambda x: 1 if x == one_label else 0)
        return self

    def scale(self, cols, method='standard'):
        scaled_vars = self._get_scaler(method).fit_transform(self.df[cols])

        self.df[cols] = scaled_vars

        return self

    def cat_to_one_hot(self, cols):
        self.df = pd.get_dummies(
            columns=cols,
            drop_first=True,
            data=self.df)

        return self

    def get_data(self):
        _features = self.df.drop(self.labels_col, axis=1)
        _labels = self.df[self.labels_col]

        return _features, _labels

    def _get_scaler(self, method):
        if method == 'standard':
            scaler = StandardScaler()
        elif method == 'min-max':
            scaler = MinMaxScaler()

        return scaler
